<?php
    namespace Recu;

class Agenda{
        
        private int $agenda_id;
        private string $name;
        private string $apellido;
        private string $cedula;
        private $datos = array();
       
 
        function __construct($aagenda_id, $aname,$aapellido,$acedula,$adatos) {
            $this->agenda = $aagenda_id;
            $this->name = $aname;
            $this->apellido = $aapellido;
            $this->cedula = $acedula;
            $this->datos= $adatos;
          
        }

        public function getAgenda() {
            return $this->agenda;
        }
    
        public function getNombre() {
            return $this->name;
        }
    
        public function getApellido() {
            return $this->apellido;
        }
    
        public function getCedula() {
            return $this->cedula;
        }
    
        public function getDatos() {
         return $this->datos;
       }   
    }

    class Telefono {
        private int $telefono_id;
        private string $telefono;
        private string $tipo;

    function __construct($atelefono_id, $atelefono,$atipo) {
        $this->telefonoid = $atelefono_id;
        $this->telefono = $atelefono;
        $this->tipo = $atipo; 
    }

    public function getTelefonoId() {
        return $this->telefonoid;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function getApellido() {
        return $this->tipo;
    }
    }
    class Email {
    private int $email_id;    
    private string $email;

    function __construct($aemail_id, $aemail) {
        $this->emailid = $aemail_id;
        $this->email = $aemail;
    }

    public function getEmailId() {
        return $this->emailid;
    }

    public function getEmail() {
        return $this->email;
    }
    }

    $emaila = new Email(11, "jazmin-18@hotmail.es");
    $agendanueva = new Agenda("1", "Jazmin", "Tucto", "4941451",array($emaila));
    $Lista = $agendanueva->getDatos();
    $agendaNombre = $agendanueva->getNombre();
  

    print("Datos de la agenda $agendaNombre son: \n");
    foreach ($Lista as $key => $value) {
        $nombreautores = $value->getEmail();
        print(" - $nombreautores \n");
    
    }
?>