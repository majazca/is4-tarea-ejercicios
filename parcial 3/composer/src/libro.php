<?php

namespace Parcial;

class Libro {
    private string $titulo;
    private $tipo;
    private string $editorial;
    private int $anho;
    private $ISBN;
    private $autores = array();

    function __construct($aTitulo, $aTipo,$aEditorial,$aAnho,$aISBN,$aautores) {
        $this->titulo = $aTitulo;
        $this->tipo = $aTipo;
        $this->editorial = $aEditorial;
        $this->anho = $aAnho;
        $this->ISBN= $aISBN;
        $this->autores= $aautores;
    }
    public function getTitulo() {
        return $this->titulo;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getEditorial() {
        return $this->editorial;
    }

    public function getYear() {
        return $this->anho;
    }

    public function getISBN() {
        return $this->ISBN;
    }

    public function getAutores() {
        return $this->autores;
    }
   
 }

 class Autor {

    private string $nombre;
    private string $nacionalidad;
    private $f_nacimiento;
    
    
    public function __construct(string $anombre, string $anacionalidad, $af_nacimiento) {
        $this->nombre = $anombre;
        $this->nacionalidad = $anacionalidad;
        $this->f_nacimiento = $af_nacimiento;
    }
    
    public function getName() {
        return $this->nombre;
    }
    public function getNacionalidad() {
        return $this->nacionalidad;
    }
    public function getNacimiento() {
        return $this->f_nacimiento;
    }
     
    }

    $autora = new Autor("Jazmin Martinez", "Paraguaya", date('d-m-Y', strtotime('15-12-1999')));
    $autorb = new Autor("Will Robinson", "Paraguayo", date('d-m-Y', strtotime('10-08-1996')));
    $libroa = new Libro("Baile", "Danza", "Fenix", 2021, "0-511054-55-1", array($autora, $autorb));
    $Lista = $libroa->getAutores();
    $libroNombre = $libroa->getTitulo();

    print("Los autores del libro $libroNombre son: \n");
    foreach ($Lista as $key => $value) {
    $nombreautores = $value->getName();
    print(" - $nombreautores \n");

}
?>