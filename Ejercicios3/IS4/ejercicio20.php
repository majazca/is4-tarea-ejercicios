<?php
/*
Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido de personas.
Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son variables que se debe introducir el usuario por formulario.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.
*/

$resultado = "";

function buscar($nombre, $apellido){
    $datos = [];
    $gestor = @fopen("agenda.txt", "r");
    if ($gestor) {
        while (($búfer = fgets($gestor, 4096)) !== false) {
            //echo $búfer."--";
            $datos[] = $búfer;
        }
        if (!feof($gestor)) {
            echo "Error: fallo inesperado de fgets()\n";
        }
        fclose($gestor);
    }
    $datosSalida = [];
    for ($i=0; $i < sizeof($datos); $i++) { 
        $datosSalida[] = explode(" ", $datos[$i]);
    }
    $i = 0;
    $encontrado = [
        'nombre' => false,
        'apellido' => false,
    ];
    while ($i < sizeof($datosSalida)) {
        if(strtoupper($nombre) == trim($datosSalida[$i][0])){
            $encontrado['nombre'] = true;
        }
        if(strtoupper($apellido) == trim($datosSalida[$i][1])){
            $encontrado['apellido'] = true;
        }
        $i++;
    }
    //print_r($datosSalida);
    $texto = "";
    if ($encontrado['nombre']) {
        $texto .= "Nombre encontrado! ";
    }
    if ($encontrado['apellido']) {
        $texto .= "Apellido encontrado! ";
    }

    if ($texto == "") {
        $texto = "Nombre y apellido no encontrados";
    }

    return $texto;
}

if (
    (isset($_GET['nombre']) || isset($_GET['apellido']))
    &&
    (!empty($_GET['nombre']) || !empty($_GET['apellido']))
    ) {
    $nombre = $_GET['nombre'];
    $apellido = $_GET['apellido'];
    //echo "cargados";
    $resultado = buscar($nombre, $apellido);
}

?>

<form action="" method="get">
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" id="nombre" placeholder="Ej: Juan">
    <label for="apellido">Apellido</label>
    <input type="text" name="apellido" id="apellido" placeholder="Ej: Perez">
    <button type="submit">Buscar!</button>
</form>
<div class="respuesta">
    <?php echo $resultado; ?>
</div>