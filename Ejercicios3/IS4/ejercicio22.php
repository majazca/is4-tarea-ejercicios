<?php

/*
Implementar un script PHP que implemente una funcionalidad básica de login
• El formulario debe tener dos campos: usuario, contraseña.
• El formulario deberá tener un botón de login.
• La verificación de usuario y contraseña se realizará a través de un archivo de datos (ya sea
.txt, .dat).
• Si el usuario introducido existe en el archivo, se debe re-enviar a una página de
bienvenida.
• Si el usuario introducido no existe en el archivo, se debe imprimir un mensaje de error de
acceso.
 */
$respuesta = "";
function obtenerAccesos(){
    $datos = [];
    $gestor = @fopen("accesos.txt", "r");
    if ($gestor) {
        while (($búfer = fgets($gestor, 4096)) !== false) {
            //echo $búfer."--";
            $datos[] = $búfer;
        }
        if (!feof($gestor)) {
            echo "Error: fallo inesperado de fgets()\n";
        }
        fclose($gestor);
    }
    $datosSalida = [];
    for ($i=0; $i < sizeof($datos); $i++) { 
        $datosSalida[] = explode(" ", $datos[$i]);
    }
    

    return $datosSalida;
}

if (
        (isset($_GET['usuario']) && isset($_GET['contrasenha']))
        &&
        (!empty($_GET['usuario']) && !empty($_GET['contrasenha']))
    ) {
    $usuario = $_GET['usuario'];
    $contrasenha = $_GET['contrasenha'];
    $datos = obtenerAccesos();
    var_dump($datos);
    echo "$usuario - $contrasenha";
    if ($usuario == $datos[0][0] && $contrasenha == $datos[0][1]) {
        echo "
        <script>
        window.location.href = './bienvenido.php?usuario=".$usuario."';
        </script>
        ";
    }else{
        $respuesta = "Error de acceso";
    }

}


?>

<form action="" method="get">
    <div>
        <label for="usuario">Usuario</label>
        <input type="text" name="usuario" id="usuario">
    </div>
    <div>
        <label for="contrasenha">Contraseña</label>
        <input type="password" name="contrasenha" id="contrasenha">
    </div>
    <button type="submit">Login</button>
</form>
<div>
    <?php echo $respuesta; ?>
</div>