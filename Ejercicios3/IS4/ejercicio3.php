<!-- Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1
y N.
• El número N estará definido por una constante PHP. El valor de la constante N debe ser un
número definido por el alumno.
El script PHP debe estar embebido en una página HTML. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>
    <?php 

        DEFINE("N",20);
        echo "constante definida = ". N;
        $data= "";
    
        for ($i=1; $i <= N; $i++) { 
            if( $i % 2 == 0) {
            $data .= "<tr> 
            <td>$i</td>
            </tr>";   
            }
           
        }

    ?>
<table border = 1>
    <thead>
        <tr>
            <td>Numero</td>
        </tr>
    </thead>
    <tbody>
        <?php echo $data; ?>
    </tbody>
</table>
</body>
</html>