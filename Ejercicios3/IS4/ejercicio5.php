<?php
  
    $formulario = <<<"JAZMIN"
    <form action="">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" placeholder="Ej: Will">
        <br>
        <label for="apellido">Apellido</label>
        <input type="text" name="apellido" id="apellido" placeholder="Ej: Tucto">
        <br>
        <label for="edad">Edad</label>
        <input type="number" name="edad" id="edad" placeholder="Ej: 15">
        <br>
        <button type="submit">Enviar</button>
    </form>
    JAZMIN;

    echo $formulario;
?>
