<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $respuesta = "";
        //print_r($_GET);
        if(isset($_GET) && sizeof($_GET)){

            $intereses = isset($_GET['intereses']) ? implode(",", $_GET['intereses']) : "";
            $colores = isset($_GET['color']) ? implode(",", $_GET['color']) : "";

            $respuesta = "
            Usuario: {$_GET['usuario']} <br>
            Contraseña: {$_GET['contrasenha']} <br>
            Año de nacimiento: {$_GET['anho']} <br>
            Lista de intereses: {$intereses} <br>
            Color favorito: {$colores} <br>
            Sexo: {$_GET['sexo']} <br>
            Frase favorita: {$_GET['frase']}
            ";
        }
    ?>
    <h3>Formulario de registro</h3>
    <form action="" method="get">
        <div>
            <label for="usuario">
                Nombre de usuario:
            </label>
            <input type="text" name="usuario" id="usuario" placeholder="Nombre de usuario">
        </div>
        <div>
            <label for="contrasenha">Contraseña:</label>
            <input type="password" name="contrasenha" id="contrasenha">
        </div>
        <div>
            <label for="anho">
                Año de nacimiento:
            </label>
            <select name="anho" id="anho">
                <?php 
                    for($i=1940;$i<2022;$i++){
                        echo "<option value='$i'>$i</option>";
                    }
                ?>
            </select>
        </div>
        <div>
            <label for="intereses">
                Intereses:
            </label>
            <select name="intereses[]" id="intereses" multiple>
                <option value="anime">Anime</option>
                <option value="deportes">Deportes</option>
                <option value="musica">Música</option>
                <option value="danza">Danza</option>
            </select>
        </div>
        <div id="colores">
            <label for="colores">Colores favoritos:</label>
            <label for="amarillo">Amarillo</label>
            <input type="checkbox" name="color[]" id="amarillo" value="Amarillo">
            <label for="rojo">Rojo</label>
            <input type="checkbox" name="color[]" id="rojo" value="Rojo">
            <label for="azul">Azul</label>
            <input type="checkbox" name="color[]" id="azul" value="Azul">
            <label for="negro">Negro</label>
            <input type="checkbox" name="color[]" id="negro" value="Negro">
            <label for="rosa">Rosa</label>
            <input type="checkbox" name="color[]" id="rosa" value="Rosa">
        </div>
        <div id="sexo">
            <label for="sexo">Sexo:</label>
            <label for="hombre">Hombre</label>
            <input type="radio" name="sexo" id="hombre" value="hombre">
            <label for="mujer">Mujer</label>
            <input type="radio" name="sexo" id="mujer" value="mujer">
        </div>
        <div>
            <div>
            <label for="frase">
                Frases favorita:
            </label>
            </div>
            <textarea name="frase" id="frase" cols="30" rows="10"></textarea>
        </div>
        <button type="submit">Guardar</button>
    </form>
    <div id="respuesta">
        <?php echo $respuesta; ?>
    </div>
</body>
</html>