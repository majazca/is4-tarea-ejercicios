<?php

    /*
    Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
    Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
    imprimir desde el último elemento del array hasta el primero
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.
    */
    $vector = [];

    for ($i=0; $i < 20; $i++) { 
        $vector[$i] = mt_rand(1,10);
    }

    function recorrerRecursivamente($array, $pos){
        echo $array[$pos]." ";
        if($pos !== 0){
            recorrerRecursivamente($array, $pos - 1);
        }
    }
    print_r($vector);
    echo "<br>";
    recorrerRecursivamente($vector, sizeof($vector)-1);

?>