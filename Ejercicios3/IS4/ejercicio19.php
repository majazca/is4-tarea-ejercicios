<?php
/*
Dado un archivo con la siguiente información: una lista de matrículas, nombre, apellido y 3 notas parciales para un grupo alumnos (el archivo puede tener un número variable de líneas de texto).
Procesar estos datos y construir un nuevo archivo que contenga solamente la matrícula y la sumatoria de notas de los alumnos.
*/
$datos = [];
$gestor = @fopen("entrada.txt", "r");
if ($gestor) {
    while (($búfer = fgets($gestor, 4096)) !== false) {
        //echo $búfer."--";
        $datos[] = $búfer;
    }
    if (!feof($gestor)) {
        echo "Error: fallo inesperado de fgets()\n";
    }
    fclose($gestor);
}

print_r($datos);

$test = explode(" ",$datos[0]);
$datosSalida = [];
for ($i=0; $i < sizeof($datos); $i++) { 
    $datosSalida[] = explode(" ", $datos[$i]);
}
echo "<br>";
print_r($datosSalida);
echo "<br>";
//echo $test[0]." ".($test[3]+$test[4]+$test[5]);
$gestor = fopen("salida.txt", "w");
for ($i=0; $i < sizeof($datosSalida); $i++) { 
    echo $datosSalida[$i][0]. " ".($datosSalida[$i][3]+$datosSalida[$i][4]+$datosSalida[$i][5])."<br>";
    fwrite($gestor, $datosSalida[$i][0]. " ".($datosSalida[$i][3]+$datosSalida[$i][4]+$datosSalida[$i][5])."".PHP_EOL);
}
fclose($gestor);



?>